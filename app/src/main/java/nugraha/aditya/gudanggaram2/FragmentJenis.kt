package nugraha.aditya.gudanggaram2

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_jenis.view.*

class FragmentJenis : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsertJns ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnDeleteJns ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnUpdateJns ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var  db: SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var  v : View
    lateinit var builder : AlertDialog.Builder

    var idJenis : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDbObject()
        v = inflater.inflate(R.layout.frag_data_jenis,container,false)
        v.btnUpdateJns.setOnClickListener(this)
        v.btnInsertJns.setOnClickListener(this)
        v.btnDeleteJns.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsJenis.setOnItemClickListener(itemClick)

        return v
    }
    fun showDataJenis(){
        val cursor : Cursor = db.query("jenis", arrayOf("nama_jenis","id_jenis as _id"),
            null,null,null,null,"nama_jenis asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_jenis,cursor,
            arrayOf("_id","nama_jenis"), intArrayOf(R.id.txIdjenis, R.id.txNamaJenis),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsJenis.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataJenis()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        idJenis = c.getString(c.getColumnIndex("_id"))
        v.edJenis.setText(c.getString(c.getColumnIndex("nama_jenis")))
    }

    fun insertDataJenis(namaJenis  : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_jenis",namaJenis)
        db.insert("jenis",null,cv)
        showDataJenis()
    }

    fun updateDataJenis(namaJenis: String, idJenis: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_jenis", namaJenis)
        db.update("jenis",cv,"id_jenis = $idJenis", null)
        showDataJenis()
    }

    fun deleteDataJenis(idjenis : String){
        db.delete("jenis","id_jenis = $idJenis",null)
        showDataJenis()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataJenis(v.edJenis.text.toString())
        v.edJenis.setText("")
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataJenis(v.edJenis.text.toString(),idJenis)
        v.edJenis.setText("")
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataJenis(idJenis)
        v.edJenis.setText("")
    }
}