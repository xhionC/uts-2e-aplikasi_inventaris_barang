package nugraha.aditya.gudanggaram2

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_brg.*
import kotlinx.android.synthetic.main.frag_data_brg.view.*

class FragmentBrg : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spBrg.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c: Cursor = spAdapter.getItem(position) as Cursor
        namaJenis = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsertBrg -> {
                dialog.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnUpdateBrg -> {
                dialog.setTitle("Konfirmasi").setMessage("Apakah data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnDeleteBrg -> {
                dialog.setTitle("Konfirmasi").setMessage("Apakah anda yakin menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v: View
    var namaBrg: String = ""
    var namaJenis: String = ""
    var kodeBrg: String = ""
    var stok : String = ""
    lateinit var db: SQLiteDatabase
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDbObject()
        v =inflater.inflate(R.layout.frag_data_brg,container,false)
        dialog = AlertDialog.Builder(thisParent)
        v.btnInsertBrg.setOnClickListener(this)
        v.btnUpdateBrg.setOnClickListener(this)
        v.btnDeleteBrg.setOnClickListener(this)
        v.spBrg.onItemSelectedListener = this
        v.lsBrg.setOnItemClickListener(itemClick)
        return v
    }

    val itemClick =  AdapterView.OnItemClickListener{parent, view, position, id ->
        val c : Cursor =  parent.adapter.getItem(position) as Cursor
        kodeBrg = c.getString(c.getColumnIndex("_id"))
        v.edNamaBrg.setText(c.getString(c.getColumnIndex("nama_brg")))
        v.edStok.setText(c.getString(c.getColumnIndex("stok")))
        v.edKodeBrg.setText(c.getString(c.getColumnIndex("_id")))
        setOf(v.spBrg.onItemSelectedListener)
    }

    fun showDataBrg() {
        var sql = ""
        sql =" select kode as _id , nama_brg , stok, nama_jenis from brg , jenis " +
                "where brg.id_jenis=jenis.id_jenis "
        val c: Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(
            thisParent,
            R.layout.item_data_brg,
            c,
            arrayOf("_id", "nama_brg", "stok", "nama_jenis"),
            intArrayOf(R.id.txIdBrg, R.id.txNamaBrg, R.id.txStok1, R.id.txJenisBrg),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lsBrg.adapter = lsAdapter
    }

    fun showDataJenis() {
        val c: Cursor =
            db.rawQuery("select nama_jenis as _id from jenis order by nama_jenis asc", null)
        spAdapter = SimpleCursorAdapter(
            thisParent,
            android.R.layout.simple_spinner_item,
            c,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spBrg.adapter = spAdapter
        v.spBrg.setSelection(0)
    }

    override fun onStart() {
        super.onStart()
        showDataBrg()
        showDataJenis()
    }

    fun insertDataBrg(kodeBrg: String, namaBrg: String, stok: String, id_jenis: Int) {
        var sql = "insert into brg(kode, nama_brg, stok, id_jenis) values (?,?,?,?)"
        db.execSQL(sql, arrayOf(kodeBrg, namaBrg, stok, id_jenis))
        showDataBrg()
    }

    fun updateDataBrg(kodeBrg: String, namaBrg: String, stok: String, id_jenis: Int){
        var cv : ContentValues = ContentValues()
        cv.put("kode",kodeBrg)
        cv.put("nama_brg",namaBrg)
        cv.put("stok",stok)
        cv.put("id_jenis",id_jenis)
        db.update("brg",cv,"kode= '$kodeBrg'",null)
        showDataBrg()

    }

    fun deleteDataBrg(kodeBrg : String){
        db.delete(" brg ","kode = '$kodeBrg'",null)
        showDataBrg()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_jenis from jenis where nama_jenis ='$namaJenis'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            insertDataBrg(
                v.edKodeBrg.text.toString(), v.edNamaBrg.text.toString(), v.edStok.text.toString(),
                c.getInt(c.getColumnIndex("id_jenis"))
            )
            v.edKodeBrg.setText("")
            v.edNamaBrg.setText("")
            v.edStok.setText("")

        }
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataBrg(kodeBrg)
        v.edKodeBrg.setText("")
        v.edNamaBrg.setText("")
        v.edStok.setText("")
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_jenis from jenis where nama_jenis ='$namaJenis'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            updateDataBrg(
                v.edKodeBrg.text.toString(), v.edNamaBrg.text.toString(), v.edStok.text.toString(),
                c.getInt(c.getColumnIndex("id_jenis"))
            )
            v.edKodeBrg.setText("")
            v.edNamaBrg.setText("")
            v.edStok.setText("")

        }

    }
}